/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const int startwithgaps[]    = { 1 };	/* 1 means gaps are used by default, this can be customized for each tag */
static const unsigned int gappx[]   = { 10 };   /* default gap between windows in pixels, this can be customized for each tag */
static const unsigned int snap      = 64;       /* snap pixel */
static const int swallowfloating    = 1;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */ static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "terminus:size=10" };
static const char dmenufont[]       = "terminus:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
/* custom privracki colours */
static const char col_voidgr[]      = "#478061";
static const char col_black[]       = "#000000";
static const char col_gray5[]       = "#dddddd";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_black, col_black },
	[SchemeSel]  = { col_black, col_gray4,  col_gray4  },
	[SchemeStatus]  = { col_gray4, col_black,  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_black, col_gray4,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
	[SchemeTagsNorm]  = { col_gray4, col_black,  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel]  = { col_gray4, col_black,  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm]  = { col_gray4, col_black,  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                     instance title tags mask iscentered, isfloating isterminal noswallow monitor */
	{ "st",                      NULL,    NULL, 0,        1,          0,         1,         0,        -1 },
	{ "Simin",                   NULL,    NULL, 0,        1,          1,         0,         1,        -1 },
	{ "Firefox",                 NULL,    NULL, 0,        0,          0,         0,         1,        -1 },
	{ "qemu-system-x86_64",      NULL,    NULL, 0,        1,          1,         0,         0,        -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "gaplessgrid.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "###",      gaplessgrid },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
	{ MODKEY|Mod4Mask,              KEY,      tagnextmon,     {.ui = 1 << TAG} }, \
	{ MODKEY|Mod4Mask|ShiftMask,    KEY,      tagprevmon,     {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_black, "-nf", col_gray4, "-sb", col_gray4, "-sf", col_black, NULL };
static const char *termcmd[]  = { "st", NULL };
/* web */
static const char *ddgcmd[] = { "/home/janko/.bin/ddg", NULL };
static const char *urlcmd[] = { "/home/janko/.bin/ffs", NULL };
/* chat */
static const char *irccmd[] = { "st", "-e", "irssi", NULL };
static const char *nhekocmd[]  = { "nheko", NULL };
static const char *telegramcmd[]  = { "telegram-desktop", NULL };
/* volume */
static const char *voldowncmd[] = { "/home/janko/.bin/volumedown", NULL };
static const char *volmutecmd[] = { "/home/janko/.bin/volumemute", NULL };
static const char *volupcmd[] = { "/home/janko/.bin/volumeup", NULL };
/* music */
static const char *playcmd[] = { "/home/janko/.bin/cmusplay", NULL };
static const char *stopcmd[] = { "cmus-remote", "-s", NULL };
static const char *prevcmd[] = { "cmus-remote", "-r", NULL };
static const char *nextcmd[] = { "cmus-remote", "-n", NULL };
/* brightness */
static const char *brightdowncmd[] = { "doas", "/home/janko/.bin/brightdown", NULL };
static const char *brightupcmd[] = { "doas", "/home/janko/.bin/brightup", NULL };

static const char *killcmd[] = { "/home/janko/.bin/killit", NULL };
static const char *mailcmd[] = { "st", "-e", "aerc", NULL };
static const char *obsdcmd[]  = { "/home/janko/.bin/obsd", NULL };
static const char *scrotfocusedcmd[] = { "scrot", "--focused", NULL };

static Key keys[] = {
	/* modifier                     key         function        argument */
	{ MODKEY,                       XK_p,       spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return,  spawn,          {.v = termcmd } },
	/* web */
	{ MODKEY,                       XK_w,       spawn,          {.v = ddgcmd } },
	{ MODKEY,                       XK_q,       spawn,          {.v = urlcmd } },
	/* chat */
	{ MODKEY,                       XK_r,       spawn,          {.v = irccmd } },
	{ MODKEY,                       XK_n,       spawn,          {.v = nhekocmd } },
	{ MODKEY,                       XK_e,       spawn,          {.v = telegramcmd } },
	/* volume */
	{ 0,                            0x1008ff11, spawn,          {.v = voldowncmd } },
	{ 0,                            0x1008ff12, spawn,          {.v = volmutecmd } },
	{ 0,                            0x1008ff13, spawn,          {.v = volupcmd } },
	/* music */
	{ 0,                            0x1008ff14, spawn,          {.v = playcmd } },
	{ 0,                            0x1008ff15, spawn,          {.v = stopcmd } },
	{ 0,                            0x1008ff16, spawn,          {.v = prevcmd } },
	{ 0,                            0x1008ff17, spawn,          {.v = nextcmd } },
	/* brightness */
	{ 0,                            0x1008ff02, spawn,          {.v = brightdowncmd } },
	{ 0,                            0x1008ff03, spawn,          {.v = brightupcmd } },

	{ MODKEY,                       XK_x,       spawn,          {.v = killcmd } },
	{ MODKEY,                       XK_a,       spawn,          {.v = mailcmd } },
	{ MODKEY,                       XK_o,       spawn,          {.v = obsdcmd } },
	{ ShiftMask,                    XK_Print,   spawn,          {.v = scrotfocusedcmd } },
	{ ControlMask,                  XK_Print,   spawn,          SHCMD("sleep 0.5; scrot --select") },
	{ 0,                            XK_Print,   spawn,          SHCMD("scrot") },
	{ MODKEY|ShiftMask,             XK_p,       spawn,          SHCMD("st -e $(dmenu_path | dmenu -i -nb \"#000\" -nf \"#EEE\" -sb \"#EEE\" -sf \"#000\")") },
	{ MODKEY,                       XK_s,       spawn,          SHCMD("st -e vim /home/janko/Documents/notes.md") },

	{ MODKEY,                       XK_b,       togglebar,      {0} },
	{ MODKEY,                       XK_j,       focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,       focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,       incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,       incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,       setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,       setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return,  zoom,           {0} },
	{ MODKEY,                       XK_Tab,     view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,       killclient,     {0} },
	{ MODKEY,                       XK_t,       setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,       setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,       setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_g,       setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_space,   setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,   togglefloating, {0} },
	{ MODKEY,                       XK_Down,    moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY,                       XK_Up,      moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY,                       XK_Right,   moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY,                       XK_Left,    moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ShiftMask,             XK_Down,    moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ShiftMask,             XK_Up,      moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ShiftMask,             XK_Right,   moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ShiftMask,             XK_Left,    moveresize,     {.v = "0x 0y -25w 0h" } },
	{ MODKEY|ControlMask,           XK_Up,      moveresizeedge, {.v = "t"} },
	{ MODKEY|ControlMask,           XK_Down,    moveresizeedge, {.v = "b"} },
	{ MODKEY|ControlMask,           XK_Left,    moveresizeedge, {.v = "l"} },
	{ MODKEY|ControlMask,           XK_Right,   moveresizeedge, {.v = "r"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Up,      moveresizeedge, {.v = "T"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Down,    moveresizeedge, {.v = "B"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Left,    moveresizeedge, {.v = "L"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Right,   moveresizeedge, {.v = "R"} },
	{ MODKEY,                       XK_0,       view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,       tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,   focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,  focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,   tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,  tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,   setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_equal,   setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,   setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_equal,   setgaps,        {.i = GAP_TOGGLE} },
	TAGKEYS(                        XK_1,                       0)
	TAGKEYS(                        XK_2,                       1)
	TAGKEYS(                        XK_3,                       2)
	TAGKEYS(                        XK_4,                       3)
	TAGKEYS(                        XK_5,                       4)
	TAGKEYS(                        XK_6,                       5)
	TAGKEYS(                        XK_7,                       6)
	TAGKEYS(                        XK_8,                       7)
	TAGKEYS(                        XK_9,                       8)
	{ MODKEY|ShiftMask,             XK_q,       quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
